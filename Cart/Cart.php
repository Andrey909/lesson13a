<?php
namespace Cart;

class Cart
{
    protected $products = [];
    public $cart;

    public function __construct()
    {
        if (isset($_COOKIE['cart'])) {
            $this->products[] = json_decode($_COOKIE['cart'], true);
        }
        return $this->products;
    }

    public  function getProducts()
    {
        foreach ($this->products as $product) {
            foreach ($product as $item) {
                echo $item. "<br>";
            }
        }
    }

    public  function saveCart()
    {
        setcookie('cart',json_encode($this->products),time()+1);
    }
    public  function addProduct($title, $product_type, $price_type, $price_val )
    {
        $this->products[]=[
            'title' => $title,
            'product_type'=>$product_type,
            'price_type' => $price_type,
            'price_val' => $price_val];
    }
}

